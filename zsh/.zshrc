setopt autocd extendedglob nomatch menucomplete 
setopt interactive_comments
zle_highlight=('paste:none')

unsetopt BEEP
# bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
# zstyle :compinstall filename '/home/praveen/.zshrc'
# Basic auto/tab complete
autoload -Uz compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

autoload -U up-line-or-begining-search
autoload -U down-line-or-begining-search
zle -N up-line-or-begining-search
zle -N down-line-or-begining-search

autoload -Uz colors && colors

source "$ZDOTDIR/zsh-functions"

zsh_add_file "async"
zsh_add_file "zsh-exports"
zsh_add_file "zsh-vim-mode"
zsh_add_file "zsh-aliases"
zsh_add_file "zsh-prompt"

zsh_add_plugin "zsh-users/zsh-autosuggestions"
zsh_add_plugin "zsh-users/zsh-syntax-highlighting"
