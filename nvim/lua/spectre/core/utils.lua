local M = {}

M.map = function(mode, lhs, rhs, opts)
  local finalRhs = ""
  local callback = nil
  if type(rhs) == "function" then
    callback = rhs
    finalRhs = ""
  else
    callback = nil
    finalRhs = rhs
  end

  opts = vim.tbl_deep_extend("force", opts, { callback = callback })
  if opts.buffer ~= nil then
    local buffer = opts.buffer
    opts.buffer = nil
    return vim.api.nvim_buf_set_keymap(buffer, mode, lhs, finalRhs, opts)
  else
    return vim.api.nvim_set_keymap(mode, lhs, finalRhs, opts)
  end
end

M.merge = function(...)
  return vim.tbl_deep_extend("force", ...)
end

M.log = function(msg, hl, name)
  name = name or "Neovim"
  hl = hl or "Todo"
  vim.api.nvim_echo({ { name .. ": ", hl }, { msg } }, true, {})
end

M.warn = function(msg, name)
  vim.notify(msg, vim.log.levels.WARN, { title = name })
end

M.info = function(msg, name)
  vim.notify(msg, vim.log.levels.INFO, { title = name })
end

M.lazyload = function(path)
  return setmetatable({}, {
    __index = function(_, key)
      return require(path)[key]
    end,
    __newindex = function(_, key, value)
      require(path)[key] = value
    end
  })
end

return M
