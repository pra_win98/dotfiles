local default_opts = { noremap = true, silent = true }
local merge = require("spectre.core.utils").merge
local map = require("spectre.core.utils").map

local default_mappings = {
  i = {
    ["jk"] = { "<ESC>" }
  },
  n = {
    ["<ESC>"] = { ":noh<CR>" },
    ["<C-j>"] = { "<C-w>j" },
    ["<C-k>"] = { "<C-w>k" },
    ["<C-h>"] = { "<C-w>h" },
    ["<C-l>"] = { "<C-w>l" },
    ["<A-j>"] = { "<C-w>-" },
    ["<A-k>"] = { "<C-w>+" },
    ["<A-h>"] = { "<C-w><" },
    ["<A-l>"] = { "<C-w>>" },
    ["<C-s>"] = { ":w!<CR>" },
    ["<C-w>"] = { ":bdelete<CR>" },
    ["<C-t>"] = { ":tabedit<CR>" },
    ["<C-a>"] = { "gg<S-v>G" },
    ["x"] = { '"_x' },
    ["+"] = { "<C-a>" },
    ["-"] = { "<C-x>" },
    ["ss"] = { ":split<CR><C-w>w" },
    ["sv"] = { ":vsplit<CR><C-w>w" },
  },
  v = {
    ["<"] = { "<gv" },
    [">"] = { ">gv" },
    ["J"] = { ":m '>+1<CR>gv=gv" },
    ["K"] = { ":m '<-2<CR>gv=gv" },
  },
  x = {
    ["J"] = { ":m '>+1<CR>gv=gv" },
    ["K"] = { ":m '<-2<CR>gv=gv" },
    ["<A-j>"] = { ":m '>+1<CR>gv=gv" },
    ["<A-k>"] = { ":m '<-2<CR>gv=gv" },
  },
}

for mode, mode_mapping in pairs(default_mappings) do
  for lhs, options in pairs(mode_mapping) do
    local rhs, opts = unpack(options)
    opts = merge(default_opts, opts or {})
    map(mode, lhs, rhs, opts)
  end
end
