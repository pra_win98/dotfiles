local modules = {
  'spectre.core.options',
  'spectre.core.keymaps',
  'spectre.core.packer',
  'spectre.core.autocmd'
}

for _, module in ipairs(modules) do
  local ok, err = pcall(require, module)
  if module == 'spectre.core.packer' and ok then
    require(module).bootstrap()
  end
  if not ok then
    error(('Error loading %s...\n\n%s'):format(module, err))
  end
end
