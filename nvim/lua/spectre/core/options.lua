vim.g.mapleader = " "
local options = {
  opt = {
    backspace = { 'eol', 'start', 'indent' },
    clipboard = { 'unnamed', 'unnamedplus' },
    encoding = 'utf-8',
    syntax = 'enable',

    autoindent = true,
    expandtab = true,
    shiftwidth = 2,
    smartindent = true,
    softtabstop = 2,
    tabstop = 2,
    ignorecase = true,
    title = true,
    shell = 'zsh',
    smartcase = true,
    cursorline = true,
    laststatus = 3,
    lazyredraw = true,
    mouse = 'a',
    number = true,
    relativenumber = true,
    scrolloff = 18,
    signcolumn = 'yes',
    splitright = true,
    splitbelow = true,
    wrap = false,
    showmode = false,

    winblend = 0,
    wildoptions = 'pum',
    pumblend = 5,
    backup = false,
    swapfile = false,
    writebackup = false,

    completeopt = { "menu", "menuone", "noinsert", "noselect" },

    redrawtime = 1500,
    timeoutlen = 250,
    ttimeoutlen = 10,
    path = { "**" },
    updatetime = 200,
    wildignore = { "*/node_modules/*" },
    termguicolors = true
  }
}


for scope, scope_opts in pairs(options) do
  for setting, value in pairs(scope_opts) do
    vim[scope][setting] = value
  end
end

local default_providers = { 'node', 'python3', 'perl', 'ruby' }

for _, provider in pairs(default_providers) do
  vim.g['loaded_' .. provider .. '_provider'] = 0
end

local disabled_built_ins = {
  'netrw',
  'netrwPlugin',
  'netrwSettings',
  'netrwFileHandlers',
  'gzip',
  'zip',
  'zipPlugin',
  'tar',
  'tarPlugin',
  'getscript',
  'getscriptPlugin',
  'vimball',
  'vimballPlugin',
  '2html_plugin',
  'logipat',
  'rrhelper',
  'spellfile_plugin',
  'matchit',
}

for _, plugin in pairs(disabled_built_ins) do
  vim.g['loaded_' .. plugin] = 1
end
