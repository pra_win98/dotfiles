local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup
local user_command = vim.api.nvim_create_user_command


local TransparentGroup = augroup("Transparent", { clear = true })
autocmd(
  { "VimEnter", "ColorScheme" },
  {
    pattern = "*",
    callback = function()
      vim.g.transparent_enabled = true
      local transparent = require("spectre.ui.transparent")
      transparent.clear_bg()
      user_command("TransparentToggle", function() transparent.toggle_transparent() end, {})
      user_command("TransparentEnable", function() transparent.toggle_transparent(true) end, {})
      user_command("TransparentDisable", function() transparent.toggle_transparent(false) end, {})
    end,
    group = TransparentGroup
  }
)

autocmd("BufEnter", {
  pattern = '*',
  command = 'set fo-=c fo-=r fo-=o'
})


autocmd("InsertLeave", {
  pattern = "*",
  command = "set nopaste"
})

autocmd({ "TextYankPost" }, {
  callback = function()
    vim.highlight.on_yank { higroup = "Visual", timeout = 200 }
  end
})
