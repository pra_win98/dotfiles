local plugins = {
  ["wbthomason/packer.nvim"] = {}, --Plugin Manager
  ['lewis6991/impatient.nvim'] = {}, --Optimizer
  ['nathom/filetype.nvim'] = {},
  ['nvim-lua/plenary.nvim'] = {},
  ['nvim-lua/popup.nvim'] = {},
  ['kyazdani42/nvim-web-devicons'] = {},
  ['nvim-treesitter/nvim-treesitter'] = {
    event = "BufWinEnter",
    config = function()
      require('spectre.configs.treesitter')
    end,
    run = ':TSUpdate'
  },

  ['nvim-treesitter/nvim-treesitter-textobjects'] = {
    after = { 'nvim-treesitter' }
  },

  ['RRethy/nvim-treesitter-textsubjects'] = {
    after = { 'nvim-treesitter' }
  },
  ["JoosepAlviste/nvim-ts-context-commentstring"] = {
    after = { 'nvim-treesitter' }
  },
  ['p00f/nvim-ts-rainbow'] = {
    after = { "nvim-treesitter" }
  },
  ['norcalli/nvim-colorizer.lua'] = {
    config = function()
      require('spectre.configs.colorizer')
    end,
  },
  ['windwp/nvim-autopairs'] = {
    config = function()
      require('spectre.configs.autopairs')
    end,
    after = "nvim-cmp"
  },

  ['windwp/nvim-ts-autotag'] = {
    after = { "nvim-treesitter" },
    config = function()
      require("spectre.configs.ts-autotag")
    end
  },
  ["hrsh7th/nvim-cmp"] = {
    config = function()
      require('spectre.configs.cmp')
    end
  },
  ["hrsh7th/cmp-nvim-lsp"] = {},
  ["hrsh7th/cmp-buffer"] = {},
  ["hrsh7th/cmp-path"] = {},
  ["saadparwaiz1/cmp_luasnip"] = {},
  ["L3MON4D3/LuaSnip"] = {},

  ["neovim/nvim-lspconfig"] = {
    config = function()
      require('spectre.configs.lsp')
    end
  },
  ["williamboman/mason.nvim"] = {},
  ["williamboman/mason-lspconfig.nvim"] = {},
  ["jose-elias-alvarez/nvim-lsp-ts-utils"] = {},
  ["jose-elias-alvarez/null-ls.nvim"] = {},

  ["nvim-telescope/telescope.nvim"] = {
    config = function()
      require('spectre.configs.telescope')
    end
  },
  ["navarasu/onedark.nvim"] = {},
  ["lewis6991/gitsigns.nvim"] = {
    config = function()
      require("spectre.configs.gitsigns")
    end
  },
  ["kyazdani42/nvim-tree.lua"] = {
    config = function()
      require("spectre.configs.nvim-tree")
    end
  },
  ["numToStr/Comment.nvim"] = {
    config = function()
      require("spectre.configs.comment")
    end
  },
  ['phaazon/hop.nvim'] = {
    branch = 'v2',
    config = function()
      require("spectre.configs.hop")
    end
  },
  ["akinsho/bufferline.nvim"] = {
    tag = "v2.*",
    event = "BufWinEnter",
    config = function()
      require("spectre.configs.bufferline")
    end
  },
  ["glepnir/lspsaga.nvim"] = {},
  ["nvim-lualine/lualine.nvim"] = {
    config = function()
      require("spectre.configs.lualine")
    end
  },
  ["rebelot/kanagawa.nvim"] = {}
}
require("spectre.core.packer").load_plugins(plugins)
