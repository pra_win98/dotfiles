local status, lualine = pcall(require, 'lualine')
if not status then return end

lualine.setup {
  options = {
    globalstatus = true,
    icons_enabled = true,
    theme = 'auto',
    component_separators = { left = '', right = '' },
    section_separators = { left = '', right = '' },
    disabled_filetypes = { "alpha", "dashboard" },
    always_divide_middle = true
  },
  tabline = {},
  extensions = {}
}
