local status_ok, nvimtree = pcall(require, "nvim-tree")
local map = require("spectre.core.utils").map

if not status_ok then
  return
end

nvimtree.setup()
map("n", "<leader>e", ":NvimTreeToggle<CR>", { noremap = true, silent = true })
