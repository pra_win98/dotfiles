local root_pattern = require("lspconfig").util.root_pattern
local servers = {
  sumneko_lua = {
    settings = {
      Lua = {
        diagnostics = {
          -- Get the language server to recognize the `vim` global
          globals = { 'vim' },
        },
        workspace = {
          -- Make the server aware of Neovim runtime files
          library = {
            [vim.fn.expand('$VIMRUNTIME/lua')] = true,
            [vim.fn.expand('$VIMRUNTIME/lua/vim/lsp')] = true,
            [vim.fn.stdpath('config') .. '/lua'] = true,
          },
          maxPreload = 10000,
        },
      },
    },
  },
  gopls = {},
  tsserver = {},
  tailwindcss = {
    root_dir = root_pattern(
      "tailwind.config.js",
      "tailwind.config.ts",
      "postcss.config.js",
      "postcss.config.ts",
      "tailwind.config.cjs"
    ),
  },
  emmet_ls = {
    filetypes = { "html", "css", "javascriptreact", "typescriptreact", "vue" },
  },
  jsonls = {
    cmd = { "vscode-json-language-server", "--stdio" },
    filetypes = { "json", "jsonc" },
    init_options = {
      provideFormatter = true,
    },
  },
  rust_analyzer = {
    root_dir = root_pattern("Cargo.toml", "rust-project.json"),
    filetypes = { "rust" }
  },
  prismals = {
    filetypes = { "prisma" }
  },
  cssls = {},
  yamlls = {
    settings = {
      yaml = {
        schemas = {
          ["https://json.schemastore.org/github-workflow.json"] = "/.github/workflows/*",
          ["../path/relative/to/file.yml"] = "/.github/workflows/*",
          ["/path/from/root/of/project"] = "/.github/workflows/*"
        },
      },
    }
  }
}

return servers
