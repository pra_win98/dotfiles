local M = {}

M.autoFormat = true

M.toggle = function()
  M.autoFormat = not M.autoFormat
  if M.autoFormat then
    print("Enabled format on save")
  else
    print("disabled format on save")
  end
end

M.format = function()
  if M.autoFormat then
    vim.lsp.buf.format()
  end
end

M.setup = function(client, buf)
  local ft = vim.api.nvim_buf_get_option(buf, "filetype")
  local nls = require("spectre.configs.lsp.null-ls")

  local enable = false

  if nls.has_formatter(ft) then
    enable = client.name == "null-ls"
  else
    enable = not (client.name == "null-ls")
  end

  client.server_capabilities.documentFormatting = enable

  if client.server_capabilities.documentFormatting then
    vim.cmd [[
      augroup LspFormat
        autocmd! * <buffer>
        autocmd BufWritePre <buffer> lua require("spectre.configs.lsp.formatting").format()
      augroup END
    ]]
  end
end

return M
