local M = {}
local merge = require("spectre.core.utils").merge
local map = require("spectre.core.utils").map
local status, saga = pcall(require, "lspsaga")
if not status then return end

saga.init_lsp_saga {
  server_filetype_map = {}
}

M.setup = function(_, bufnr)
  local default_opts = { noremap = true, silent = true, buffer = bufnr }

  local mappings = {
    n = {
      ["gD"] = { "<cmd>lua vim.lsp.buf.declaration()<CR>" },
      ["gd"] = { "<cmd>Lspsaga lsp_finder<CR>" },
      ["K"] = { "<cmd>Lspsaga hover_doc<CR>" },
      ["gp"] = { "<cmd>Lspsaga preview_defination<CR>" },
      ["[d"] = { '<cmd>lua vim.diagnostic.goto_prev({ border = "rounded" })<CR>' },
      ["gl"] = { '<cmd>lua vim.diagnostic.open_float({ border = "rounded" })<CR>' },
      ["]d"] = { '<cmd>lua vim.diagnostic.goto_next({ border = "rounded" })<CR>' },
      ["<leader>q"] = { "<cmd>Lspsaga diagnostic_jump_next<CR>" },
      ["<leader>a"] = { "<cmd>Lspsaga code_action<CR>" },
      ["<leader>rn"] = { '<cmd>Lspsaga rename<CR>' }
    },
    i = {
      ["<C-k>"] = { "<cmd>Lspsaga signature_help<CR>" },
    }
  }

  for mode, mode_mapping in pairs(mappings) do
    for lhs, options in pairs(mode_mapping) do
      local rhs, opts = unpack(options)
      opts = merge(default_opts, opts or {})
      map(mode, lhs, rhs, opts)
    end
  end
end

return M
