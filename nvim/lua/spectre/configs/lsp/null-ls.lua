local M = {}

M.setup = function(options)
  local nls = require("null-ls")
  local formatting = nls.builtins.formatting
  nls.setup({
    debounce = 150,
    save_after_format = false,
    sources = {
      formatting.prettier
    },
    on_attach = options.on_attach,
    root_dir = require("null-ls.utils").root_pattern(".null-ls-root", ".nvim.settings.json", ".git")
  })
end

M.has_formatter = function(ft)
  local sources = require("null-ls.sources")
  local available = sources.get_available(ft, "NULL_LS_FORMATTING")
  return #available > 0
end

return M
