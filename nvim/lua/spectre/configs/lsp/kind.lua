local M = {}
local icons = require('spectre.ui.icons').cmp_kind

M.setup = function()
  local kinds = vim.lsp.protocol.CompletionItemKind
  for i, kind in ipairs(kinds)do
    kinds[i] = icons[kind] or kind
  end
end

return M
