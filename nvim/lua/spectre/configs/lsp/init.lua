local servers = require("spectre.configs.lsp.servers")

require("spectre.configs.lsp.diagnostics").setup()
require("spectre.configs.lsp.kind").setup()
require("mason").setup({
  ui = {
    icons = {
      package_pending = " ",
      package_installed = " ",
      package_uninstalled = " ﮊ",
    },
  }
})
require("mason-lspconfig").setup({
  ensure_installed = { "sumneko_lua", "gopls", "tsserver" },
  automatic_installation = true
})

local function on_attach(client, bufnr)
  require("spectre.configs.lsp.formatting").setup(client, bufnr)
  require("spectre.configs.lsp.keymaps").setup(client, bufnr)
  if client.name == 'typescript' or client.name == 'tsserver' then
    require("spectre.configs.lsp.ts-utils").setup(client)
  end
end

local capabilities = require("cmp_nvim_lsp").update_capabilities(vim.lsp.protocol.make_client_capabilities())

local options = {
  on_attach = on_attach,
  capabilities = capabilities,
  flags = {
    debounce_text_changes = 150
  }
}

for server, opts in pairs(servers) do
  opts = vim.tbl_deep_extend("force", {}, options, opts or {})
  require("lspconfig")[server].setup(opts)
end

require("spectre.configs.lsp.null-ls").setup(options)
