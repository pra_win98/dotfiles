local has_impatient, impatient = pcall(require, "impatient")
if has_impatient then
  impatient.enable_profile()
end
require("spectre")
-- local has_theme, onedark = pcall(require, "onedark")
-- if has_theme then
--   onedark.setup {
--     style = 'deep'
--   }
--   onedark.load()
-- end
vim.cmd [[colorscheme kanagawa]]
